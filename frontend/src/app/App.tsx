import React from 'react';
import { Layout } from '../layout/Layout';
import './App.css';

export class App extends React.Component {
  render() {
    return (
      <div className="App">
        <Layout />
      </div>
    );
  }
}

