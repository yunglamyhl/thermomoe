import React from "react";
import { ThermomeChart } from "../chart/ThermomeChart";

interface IProps {
}

interface IState {
}

export class Layout extends React.Component<IProps, IState> {
    
    constructor(props: IProps) {
        super(props);
    }

    render() {
        return (
            <div>
                <ThermomeChart type="bar" />
            </div>
        )
    }
}