import React from "react";
import Chart from "react-apexcharts";

interface IProps {
    type: "line" | "area" | "bar" | "histogram" | "pie" | "donut" |
    "radialBar" | "scatter" | "bubble" | "heatmap" | "candlestick" | "radar"
}

interface IState {
    options: { chart: { id: string}, xaxis: { categories: number[] } }
    series: { name: string, data: number[] }[]
}

export class ThermomeChart extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);

        this.state = {
            options: {
                chart: {
                    id: "basic-bar"
                },
                xaxis: {
                    categories: [1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999]
                }
            },
            series: [
                {
                    name: "series-1",
                    data: [30, 40, 45, 50, 49, 60, 70, 91]
                }
            ],
            
        };
    }

    render() {
        const { type } = this.props;
        const { options, series } = this.state;
        return (
            <div>
                <Chart
                    options={options}
                    series={series}
                    type={type}
                />
            </div>
        )
    }
}