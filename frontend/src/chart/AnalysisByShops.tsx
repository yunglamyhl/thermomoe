import React from 'react';
import ReactApexChart from 'react-apexcharts';

// interface IShopsState {

// }
function generateDayWiseTimeSeries(baseval: number, count: number, yrange: { min: any; max: any; }) {
    var i = 0;
    var series = [];
    while (i < count) {
      var y = Math.floor(Math.random() * (yrange.max - yrange.min + 1)) + yrange.min;

      series.push([baseval, y]);
      baseval += 86400000;
      i++;
    }
    return series;
  }

export class AnalysisByShops extends React.Component<any, any> {
    constructor(props: any) {
        super(props);

        this.state = {
            options: {
                chart: {
                  zoom: {
                    type: 'xy'
                  }
                },
                dataLabels: {
                  enabled: false
                },
                grid: {
                  xaxis: {
                    showLines: true
                  },
                  yaxis: {
                    showLines: true
                  },
                },
                xaxis: {
                  type: 'datetime',
                },
                yaxis: {
                  type: 'datetime',
                }
              },
              series: [
                {
                  name: 'Ceiling',
                  data: generateDayWiseTimeSeries(new Date('01 Sep 2019 GMT').getTime(), 20, {
                    min: 10,
                    max: 60
                  })
                },
                {
                  name: 'Floor',
                  data: generateDayWiseTimeSeries(new Date('07 Sep 2019 GMT').getTime(), 20, {
                    min: 10,
                    max: 60
                  })
                },
            ],
        }
    }
    render() {
        console.log(this.state)
        return (
            <div id="chart">
            <ReactApexChart options={this.state.options} series={this.state.series} type="scatter" height="350" />
          </div>
        )
    }
}

