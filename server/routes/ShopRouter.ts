import { Router, Request, Response } from 'express'
import { ShopService } from '../services'

export class ShopRouter {
    constructor(private shopService: ShopService) {}

    public getRoutes = () => {
        const router = Router()
        router.get('/', this.getAll)
        router.get('/:id', this.getById)
        router.post('/', this.create)
        router.put('/', this.update)
        return router
    }

    private getAll = async (_req: Request, res: Response) => {
        try {
            const result = await this.shopService.get()
            res.json(result)
        } catch(e) {
            console.error(e)
            res.status(400).json({ msg: '@shop/GET_ALL_FAILED' })
        }
    }

    private getById = async (req: Request, res: Response) => {
        try {
            const { id } = req.params
            if (!isNaN(parseInt(id))) {
                const result = await this.shopService.get(parseInt(id))
                res.json({ result })
            }
            throw new Error("@shop/INVALID_INPUT")
        } catch(e) {
            console.error(e)
            res.status(400).json({ msg: '@shop/GET_BY_ID_FAILED' })
        }
    }

    private create = async (req: Request, res: Response) => {
        try {
            const { name, locationId } = req.body
            if (name && !isNaN(parseInt(locationId))) {
                const result = await this.shopService.create(name, locationId)
                res.json({ result })
            }
            throw new Error("@shop/INVALID_INPUT")
        } catch(e) {
            console.error(e)
            res.status(400).json({ msg: '@shop/CREATE_FAILED' })
        }
    }

    private update = async (req: Request, res: Response) => {
        try {
            const { id, name, locationId } = req.body
            if (!isNaN(parseInt(id)) && name && !isNaN(parseInt(locationId))) {
                const result = await this.shopService.update(id, name, locationId)
                res.json({ result })
            }
            throw new Error("@shop/INVALID_INPUT")
        } catch(e) {
            console.error(e)
            res.status(400).json({ msg: '@shop/UPDATE_FAILED' })
        }
    }
}
