import * as Knex from "knex";
import { ISensor } from "../models/Sensor";

export class SensorService {
    private readonly TABLE_NAME = 'shops'

    constructor(private knex: Knex) {}

    public get = async (id: number | null = null): Promise<ISensor> => {
        const queryBuilder = this.knex.select<ISensor>({
            id: "id",
            sensorId: "sensor_id",
            timeSlot: "time_slot",
            temperature: "temperature"
        }).from(this.TABLE_NAME)

        if (id) {
            queryBuilder.where('id', id)
        }

        return await queryBuilder
    }

    public create = async (sensorId: number, timeSlot: number, temperature: number): Promise<number[]> => {
        return await this.knex.insert({
            sensor_id: sensorId,
            time_slot: timeSlot,
            temperature: temperature
        }).into(this.TABLE_NAME).returning('id')
    }

    public update = async (id: number, sensorId: number, timeSlot: number, temperature: number): Promise<number> => {
        return await this.knex(this.TABLE_NAME).update({
            sensor_id: sensorId,
            time_slot: timeSlot,
            temperature: temperature
        }).where('id', id)
    }
}
