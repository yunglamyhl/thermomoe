import * as Knex from "knex";
import { IShop } from "../models/Shop";

export class ShopService {
    private readonly TABLE_NAME = 'shops'

    constructor(private knex: Knex) {}

    public get = async (id: number | null = null): Promise<IShop> => {
        const queryBuilder = this.knex.select<IShop>({
            id: "id",
            name: "name",
            locationId: "location_id"
        }).from(this.TABLE_NAME)

        if (id) {
            queryBuilder.where('id', id)
        }

        return await queryBuilder
    }

    public create = async (name: string, locationId: number): Promise<number[]> => {
        return await this.knex.insert({
            name,
            location_id: locationId
        }).into(this.TABLE_NAME).returning('id')
    }

    public update = async (id: number, name: string, locationId: number): Promise<number> => {
        return await this.knex(this.TABLE_NAME).update({
            name,
            location_id: locationId
        }).where('id', id)
    }
}
