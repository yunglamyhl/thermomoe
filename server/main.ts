import express from 'express';
import Knex from 'knex';
import { config } from 'dotenv';
import * as bodyparser from 'body-parser';
import { ShopService, LocationService } from './services';
import { ShopRouter, LocationRouter } from './routes';

config();
const { PORT, NODE_ENV } = process.env;

const knexConfig = require('./knexfile');
const knex = Knex(knexConfig[NODE_ENV || 'development'])

const shopService = new ShopService(knex);
const shopRouter = new ShopRouter(shopService);

const locationService = new LocationService(knex);
const locationRouter = new LocationRouter(locationService);

// express
const app = express();
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: false }));
app.use('/shop', shopRouter.getRoutes());
app.use('/location', locationRouter.getRoutes());
app.use('/location', locationRouter.getRoutes());

app.listen(PORT, () => {
    console.log(`Application is started at ${PORT}`)
});
